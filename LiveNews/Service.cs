﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace LiveNews
{
    public class Service
    {
        public async Task<List<Article>> GetArticlesByCategoryFromAPI(string cat) 
        {
			try
			{
                var client = new HttpClient();
                client.DefaultRequestHeaders.Add("user-agent", "News-API-csharp/0.1");
                client.DefaultRequestHeaders.Add("x-api-key", "27c592a483de432fb8b47b4ef80d6114");

                var url = new Uri(string.Format("https://newsapi.org/v2/top-headlines?category=" + cat + "&language=en", string.Empty));
                HttpResponseMessage response = await client.GetAsync(url);
                if (response.IsSuccessStatusCode)
                {
                    string contentResult = await response.Content.ReadAsStringAsync();
                    var result = JsonSerializer.Deserialize<APIResponse>(contentResult, new JsonSerializerOptions
                    {
                        PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                        WriteIndented = true
                    });

                    if(result != null && result.Articles != null)
                        return result.Articles;
                }
                return null;
                

            }
            catch (Exception e)
			{

				throw;
			}
        }

        public async Task<List<Article>> GetArticlesBySearchFromAPI(string seachText)
        {
            try
            {
                var client = new HttpClient();
                client.DefaultRequestHeaders.Add("user-agent", "News-API-csharp/0.1");
                client.DefaultRequestHeaders.Add("x-api-key", "27c592a483de432fb8b47b4ef80d6114");

                var url = new Uri(string.Format("https://newsapi.org/v2/top-headlines?q=" + seachText + "&language=en", string.Empty));
                HttpResponseMessage response = await client.GetAsync(url);
                if (response.IsSuccessStatusCode)
                {
                    string contentResult = await response.Content.ReadAsStringAsync();
                    var result = JsonSerializer.Deserialize<APIResponse>(contentResult, new JsonSerializerOptions
                    {
                        PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                        WriteIndented = true
                    });

                    if (result != null && result.Articles != null)
                        return result.Articles;
                }
                return null;


            }
            catch (Exception e)
            {

                throw;
            }
        }
    }
}
