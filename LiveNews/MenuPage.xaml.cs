namespace LiveNews;

public partial class MenuPage : ContentPage
{
	ViewModel vModel;
	public MenuPage(ViewModel vModel)
	{
		this.vModel = vModel;
		InitializeComponent();
		this.BindingContext = vModel;
	}

    private async void SearchBar_SearchButtonPressed(object sender, EventArgs e)
    {


        await vModel.GetArticlesBySearch(searchText.Text);
		await this.Navigation.PopAsync();
    }

    private async void CategoryListItemChanged(object sender, SelectionChangedEventArgs e)
    {
        await vModel.GetArticles(((CatView)e.CurrentSelection.FirstOrDefault()).Name.ToLower());
        await this.Navigation.PopAsync();
    }
}