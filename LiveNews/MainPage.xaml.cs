﻿namespace LiveNews;

public partial class MainPage : ContentPage
{
	int count = 0;
	ViewModel vmodel = new ViewModel();
	public MainPage()
	{
		InitializeComponent();
		this.BindingContext = vmodel;
        GetData();
	}
    public async void GetData()
    {
        await vmodel.GetArticles("general");
    }
    protected async override void OnAppearing()
    {
		
    }

    private async void CategoryListItemChanged(object sender, SelectionChangedEventArgs e)
    {
        await vmodel.GetArticles(((CatView)e.CurrentSelection.FirstOrDefault()).Name.ToLower());
    }



    private void Top5_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
        vmodel.Article = (Article)e.CurrentSelection.FirstOrDefault();
        this.Navigation.PushAsync(new DetailPage(vmodel));
    }

    private void MenuClicked(object sender, EventArgs e)
    {
        this.Navigation.PushAsync(new MenuPage(vmodel));
    }
}

