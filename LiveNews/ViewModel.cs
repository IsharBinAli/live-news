﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;


namespace LiveNews
{
    public class ViewModel : INotifyPropertyChanged
    {
        public ViewModel()
        {
            CategoryList = new List<CatView>
            {
               new CatView { Name = "General",icon="heart.jpeg" },  
               new CatView { Name = "Business", icon = "businesss.png" },
               new CatView { Name = "Entertainment", icon = "entertaimnet.png" },
               new CatView { Name = "Health", icon = "heart.png" },
               new CatView { Name = "Science", icon = "sciences.png" },
               new CatView { Name = "Sports", icon = "sport.png" },
               new CatView { Name = "Technology", icon = "ai" }
            };
        }

        private ObservableCollection<Article> articles;

        public ObservableCollection<Article> Articles
        {
            get { return articles; }
            set { articles = value; OnPropertyChanged(); }
        }

        private Article article;

        public Article Article
        {
            get { return article; }
            set { article = value; OnPropertyChanged(); }
        }

        private ObservableCollection<Article> top5;

        public ObservableCollection<Article> Top5
        {
            get { return top5; }
            set { top5 = value; OnPropertyChanged(); }
        }

        private List<CatView> categoryList;

        public List<CatView> CategoryList
        {
            get { return categoryList; }
            set { categoryList = value; OnPropertyChanged(); }
        }


        public async Task GetArticles(string category)
        {
            var data =await new Service().GetArticlesByCategoryFromAPI(category);
            if (data != null)
            {
                Articles = new ObservableCollection<Article>(data);
                Top5 = new ObservableCollection<Article>(data.Take(5).ToList());
            }
  
        }

        public async Task GetArticlesBySearch(string search)
        {
            var data = await new Service().GetArticlesBySearchFromAPI(search);
            if (data != null)
            {
                Articles = new ObservableCollection<Article>(data);
                Top5 = new ObservableCollection<Article>(data.Take(5).ToList());
            }

        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string name = "") =>
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
    }

    public class CatView
    {
        public string Name { get; set; }
        public string icon { get; set; }
    }
}
