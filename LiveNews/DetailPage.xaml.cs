namespace LiveNews;

public partial class DetailPage : ContentPage
{
	public DetailPage(ViewModel vmodel)
	{
		InitializeComponent();
		this.BindingContext = vmodel;
	}
}