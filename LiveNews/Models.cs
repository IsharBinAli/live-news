﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiveNews
{
    public class Article
    {
        public string Author { get; set; }
        public string Description { get; set; }
        public DateTime PublishedAt { get; set; }
        public string Title { get; set; }
        public string URL { get; set; }
        public string URLToImage { get; set; }
        public string Content { get; set; }


    }

    public class APIResponse
    {
        public List<Article> Articles { get; set; }
        public string Status { get; set; }
        public string TotalRecords { get; set; }

    }
}
